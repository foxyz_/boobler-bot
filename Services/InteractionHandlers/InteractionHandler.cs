﻿using System.Text.RegularExpressions;
using BooblerBot.Context;
using Discord;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;

namespace BooblerBot.Services.InteractionHandlers; 

public partial class InteractionHandler {
    readonly DiscordSocketClient client;
    readonly HttpClient httpClient;
    readonly MyDbContext ctx;

    public InteractionHandler(DiscordSocketClient c, MyDbContext ctx, HttpClient httpClient, WeebService weebService) {
        client = c;
        this.ctx = ctx;
        this.httpClient = httpClient;
        this.weebService = weebService;
    }

    public async Task HandleSlashCommand(SocketSlashCommand cmd) {
        switch (cmd.Data.Name) { 
            case "week": await HandleWeekCommand(cmd); break;
            //case "sd": await HandleSdCommands(); break;
            //case "set-announcement": await DiscordBot.SetAnnouncementChannel(cmd); break;
            //case "set-vk": await DiscordBot.SetVkChannel(cmd); break;
            //case "gel": await Posts.HandleBooblers(cmd); break;
            case "weeb": await HandleWeebCommands(cmd); break;
            // case "check": await HandleCheckCommands(); break;
            //case "profile": await Eyebrower.HandleProfileCommand(cmd); break;
            //case "shop": await Eyebrower.HandleShopCommand(cmd); break;
            //case "leaderboard": await Eyebrower.HandleBoardCommand(cmd); break;
            default: Console.WriteLine($"Unhandled Command ({cmd.Data.Name})");
                await cmd.RespondAsync($"Unhandled Command ({cmd.Data.Name})", ephemeral: true); break;
        }
    }

    string ExtractTitleNameFromFirstEpisodeAnnouncement(SocketMessageComponent arg) {
        var match = new Regex("Came out the first episode of \\\"(.*)\\\"").Match(arg.Message.Embeds.First().Title);
        string? name = match.Groups[1].Value;

        if (name == null) {
            throw new Exception("There was a fatal error extracting data out of the first episode announcement!");
        }

        return name;
    }
    void BuildFirstEpisodeMessage(MessageProperties p, ServerData dbServer, Title dbTitle, string thumbnailUrl) {
        string? guildName = client.Guilds.First(g => g.Id == dbServer.Id).Name;
        bool isTracked = dbServer.Titles.Any(title => title.Name == dbTitle.Name);
        
        var embed = new EmbedBuilder {
            Title = $"Came out the first episode of \"{dbTitle.Name}\"",
            Color = new Color(69, 136, 230),
            Description = $"{(isTracked ? "🟢 Is" : "🔴 Is not")} tracked on {guildName}",
            ThumbnailUrl = thumbnailUrl,
        };
        
        
        var episode = dbTitle.Episodes.First();
        var fittingButton = isTracked ? new ButtonBuilder("Untrack", "first-episode-untrack") : new ButtonBuilder("Track", "first-episode-track");

        embed.AddField("**Torrent**", $"[__**Download!**__]({"https://nyaasi.final-spark.eu" + episode.Link[15..]})");
        embed.WithFooter(episode.PubDate.ToString("ddd, dd MMM yyy HH:mm:ss"));
        var components = new ComponentBuilder().WithButton(fittingButton).Build();

        p.Embed = embed.Build(); 
        p.Components = components;
    }
    async Task FirstEpisodeTrackTitle(SocketMessageComponent arg) {
        await arg.DeferAsync();
        string titleName = ExtractTitleNameFromFirstEpisodeAnnouncement(arg);
        
        var dbServer = ctx.servers.Where(s => s.Id == arg.GuildId)
            .Include(s => s.Titles)
            .First();
        var dbTitle = ctx.titles.Where(t => t.Name == titleName)
            .Include(t => t.Episodes)
            .First(); 
        
        await Track(arg, dbServer, dbTitle);
        string coverPicUrl = await GetCoverPic(httpClient, dbTitle.Name);
        await arg.ModifyOriginalResponseAsync(p =>  BuildFirstEpisodeMessage(p, dbServer, dbTitle, coverPicUrl));
    }
    async Task FirstEpisodeUntrackTitle(SocketMessageComponent arg) {
        await arg.DeferAsync();
        string titleName = ExtractTitleNameFromFirstEpisodeAnnouncement(arg);
        
        var dbServer = ctx.servers.Where(s => s.Id == arg.GuildId)
            .Include(s => s.Titles)
            .First();
        var dbTitle = ctx.titles.Where(t => t.Name == titleName)
            .Include(t => t.Episodes)
            .First(); 
        
        await Untrack(arg, dbServer, dbTitle);
        string coverPicUrl = await GetCoverPic(httpClient, dbTitle.Name);
        await arg.ModifyOriginalResponseAsync(p => BuildFirstEpisodeMessage(p, dbServer, dbTitle, coverPicUrl));
    }
    
    public Task HandleButton(SocketMessageComponent arg) {
        string route = arg.Data.CustomId.Split('/').First();
        return route switch {
            //"eybrow" => Eyebrower.HandleButtons(arg),
            "anime-selector-track" => SelectorTrackTitle(arg),
            "anime-selector-untrack" => SelectorUntrackTitle(arg),
            "first-episode-track" => FirstEpisodeTrackTitle(arg), 
            "first-episode-untrack" => FirstEpisodeUntrackTitle(arg),
            "all-episodes" => ShowAllEpisodes(arg),
            _ => Task.Run(() => {
                Console.WriteLine($"Unhandled button interaction ({route})"); arg.RespondAsync($"Unhandled button interaction ({route})", ephemeral: true); })
        };
    }
    public Task HandleSelectMenu(SocketMessageComponent arg) {
        string route = arg.Data.CustomId.Split('/').First();
        return route switch {
            "anime-selector" => HandleAnimeSelectMenu(arg),
            "anime-page-selector" =>  HandlePageSelectMenu(arg),
            _ => Task.Run(() => {
                Console.WriteLine($"Unhandled select menu interaction ({route})"); arg.RespondAsync($"Unhandled select menu interaction ({route})", ephemeral: true); })
        };
    }
    static Embed Embeddber(string title, string? descripto = null) {
        return new EmbedBuilder {
            Title = title,
            Description = descripto,
            Color = new Color(107, 50, 168)
        }.Build();
    }

}