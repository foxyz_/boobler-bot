﻿using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using BooblerBot.Context;
using BooblerBot.Models.API_Models;
using BooblerBot.TimmyUtils;
using Discord;
using Discord.WebSocket;
using HtmlAgilityPack;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static System.String;

namespace BooblerBot.Services.InteractionHandlers; 

partial class InteractionHandler {
    readonly WeebService weebService;
        
    async Task SelectorTrackTitle(SocketMessageComponent arg) {
        await arg.DeferAsync();
        
        // Get page for page selector
        var sm = (arg.Message.Components.ElementAt(1).Components.First() as SelectMenuComponent)!;
        int page = int.Parse(sm.Options.First(o => o.IsDefault != null && o.IsDefault.Value).Value);
        
        // Get title name for dbTitle
        string name = arg.Message.Embeds.First().Title;

        // Get current seasonal titles for screen, title for screen and selector
        var seasonalTitles = GetRelevantTitles(page);
        var server = ctx.servers.Where(s => s.Id == arg.GuildId).Include(s => s.Titles).First();
        var title = ctx.titles.Where(t => t.Name == name).Include(t => t.FollowingServers).First(); //Include following servers only for this title
        
        // Tracking
        await Track(arg, server, title);
        
        // Update UI | Has to know DB was already updated
        bool isTracked = title.FollowingServers.Contains(server);
        var fittingButton = isTracked ? new ButtonBuilder("Untrack", "anime-selector-untrack") : new ButtonBuilder("Track", "anime-selector-track"); 
        var allEpisodes = new ButtonBuilder("All episodes", "all-episodes"); 
        var componenets = new ComponentBuilder().WithSelectMenu(AnimeSelector(seasonalTitles, server, title.Name)).WithSelectMenu(PageSelector(page))
            .AddRow(new ActionRowBuilder().WithButton(fittingButton).WithButton(allEpisodes));
        var screen = await Screen(arg.GuildId!.Value, title, isTracked);
        await arg.ModifyOriginalResponseAsync(p => { p.Embed = screen; p.Components = componenets.Build(); });
    }

    async Task SelectorUntrackTitle(SocketMessageComponent arg) {
        await arg.DeferAsync();

        // Get page for page selector
        var sm = (arg.Message.Components.ElementAt(1).Components.First() as SelectMenuComponent)!;
        int page = int.Parse(sm.Options.First(o => o.IsDefault != null && o.IsDefault.Value).Value);
        
        // Get title name for dbTitle
        string name = arg.Message.Embeds.First().Title;

        // Get current seasonal titles for screen, title for screen and selector
        var seasonalTitles = GetRelevantTitles(page);
        var server = ctx.servers.Where(s => s.Id == arg.GuildId).Include(s => s.Titles).First();
        var title = ctx.titles.Where(t => t.Name == name).Include(t => t.FollowingServers).First(); //Include following servers only for this title

        // Untrack
        await Untrack(arg, server, title);
        
        // Update UI | Has to know DB was already updated
        bool isTracked = title.FollowingServers.Contains(server);
        var fittingButton = isTracked ? new ButtonBuilder("Untrack", "anime-selector-untrack") : new ButtonBuilder("Track", "anime-selector-track");
        var allEpisodes = new ButtonBuilder("All episodes", "all-episodes"); 
        var componenets = new ComponentBuilder().WithSelectMenu(AnimeSelector(seasonalTitles, server, title.Name)).WithSelectMenu(PageSelector(page))
            .AddRow(new ActionRowBuilder().WithButton(fittingButton).WithButton(allEpisodes));
        var screen = await Screen(arg.GuildId!.Value, title, isTracked);
        await arg.ModifyOriginalResponseAsync(p => { p.Embed = screen; p.Components = componenets.Build(); });
    }
    async Task Track(SocketInteraction arg, ServerData dbServer, Title dbTitle) {
        string? serverName = client.Guilds.First(g => g.Id == dbServer.Id).Name;
        
        if (dbServer.Titles.Contains(dbTitle)) {
            Console.WriteLine($"{serverName} is already following {dbTitle.Name}!");
            await arg.FollowupAsync($"{serverName} is already following {dbTitle.Name}!\nIf you see this message report it to whoever made ths shit.", ephemeral: true);
            return;
        }
        
        dbTitle.FollowingServers.Add(dbServer);
        await GetMissingEpisodes(dbTitle.Name);
        await ctx.SaveChangesAsync();
        await AnnounceNewFollow(dbTitle, serverName, dbServer);
    }
    async Task Untrack(SocketInteraction arg, ServerData dbServer, Title dbTitle) {
        string? serverName = client.Guilds.First(g => g.Id == dbServer.Id).Name;
        if (!dbServer.Titles.Contains(dbTitle)) {
            Console.WriteLine($"{serverName} is not even following {dbTitle.Name}!");
            await arg.FollowupAsync($"{serverName} is not even following {dbTitle.Name}!\nIf you see this message report it to whoever made ths shit.", ephemeral: true);
            return;
        }
        dbServer.Titles.Remove(dbTitle);
        await ctx.SaveChangesAsync();
        await AnnounceUnfollow(dbTitle, serverName, dbServer);
    }
    async Task AnnounceNewFollow(Title title, string serverName, ServerData server) {
        title = ctx.titles.Include(t => t.Episodes).First(t => t.Id == title.Id);
        
        Console.WriteLine($"{serverName} is now following {title.Name}");
        
        var embed = new EmbedBuilder {
            Title = $"{serverName} is now following \"{title.Name}\"",
            Color = new Color(69, 136, 230),
            ThumbnailUrl = await GetCoverPic(httpClient, title.Name),
        };
        StringBuilder sb = new();
        foreach (var episode in title.Episodes.TakeLast(12)) {
            sb.AppendLine($"[__**Episode {episode.Number}**__]({"https://nyaasi.final-spark.eu" + episode.Link[15..]})");
        }
        embed.AddField("**Download torrent:**", sb.ToString());
        embed.WithFooter(DateTime.Now.ToString("ddd, dd MMM yyy HH:mm:ss"));
        await weebService.AnnouncementChannels.First(pair => pair.Key == server.Id).Value.SendMessageAsync(embed: embed.Build());
    }    
    async Task AnnounceUnfollow(Title title, string serverName, ServerData server) {
        title = ctx.titles.Include(t => t.Episodes).First(t => t.Id == title.Id);
        
        Console.WriteLine($"{serverName} is no longer following {title.Name}");
        
        var embed = new EmbedBuilder {
            Title = $"{serverName} is no longer following \"{title.Name}\"",
            Color = new Color(69, 136, 230),
            ThumbnailUrl = await GetCoverPic(httpClient, title.Name),
        };
        
        embed.WithFooter(DateTime.Now.ToString("ddd, dd MMM yyy HH:mm:ss"));
        await weebService.AnnouncementChannels.First(pair => pair.Key == server.Id).Value.SendMessageAsync(embed: embed.Build());
    }


        
    async Task HandleWeekCommand(SocketSlashCommand cmd) {
        try {
            var builder = new StringBuilder[7];
            foreach (var title in ctx.titles.Include(t => t.Episodes).Include(t => t.FollowingServers)) {
                if (!title.Episodes.Any() || title.FollowingServers.All(server => server.Id != cmd.GuildId)) continue;
                var lastEpisode = title.Episodes.Last();
                
                // Check if the last episode from the anime came out within 8 days
                if (Timmy.TimeSince(lastEpisode.PubDate) > TimeSpan.FromDays(8)) continue;
                
                int dayIndex = Timmy.DayOfWeek(lastEpisode.PubDate);
                builder[dayIndex] ??= new StringBuilder();
                builder[dayIndex].AppendLine(
                    $"`{Regex.Replace(lastEpisode.Title.Name, @"^((?:\S+\s+){2}\S+).*", "${1}")}` <t:{lastEpisode.PubDate.GetUnixTimeStamp()}:t> <t:{lastEpisode.PubDate.GetUnixTimeStamp()}:R>");
            }

            // Adding custom titles
            foreach (var title in ctx.custom_titles) {
                int dayIndex = Timmy.DayOfWeek(title.DayOfWeek);
                builder[dayIndex] ??= new StringBuilder();
                builder[dayIndex].AppendLine(
                    $"`{Regex.Replace(title.Name, @"^((?:\S+\s+){2}\S+).*", "${1}")}` <t:{title.DayOfWeek.GetUnixTimeStamp()}:t> `Custom`");
            }

            var megaBuilder = new StringBuilder();
            for (int i = 0; i < builder.Length; i++) {
                if (builder[i] == null) continue;
                //megaBuilder.AppendLine($"{new CultureInfo("ru-RU").DateTimeFormat.GetDayName(Timmy.WeekOfDay(i))}:");
                megaBuilder.AppendLine($"{Timmy.WeekOfDay(i)}:");
                megaBuilder.AppendLine(builder[i].ToString());
            }
            
            await cmd.RespondAsync(embed: Embeddber("Weekly anime schedule", megaBuilder.ToString()), ephemeral: true);
        }
        catch (Exception e) {
            Console.WriteLine(e);
            throw;
        }
    }

    async Task HandleWeebCommands(SocketSlashCommand cmd) {
        switch (cmd.Data.Options.First().Name) {
            case "menu": await Menu(cmd); break;
            default: Console.WriteLine($"Unhandled command: {cmd.Data.Options.First().Name}"); break;
        }
    }
    async Task Menu(SocketSlashCommand cmd) {
        var seasonalTitles = GetRelevantTitles();
        var server = ctx.servers.Where(s => s.Id == cmd.GuildId).Include(s => s.Titles).First();
        var title = seasonalTitles.First();
    
        bool isTracked = title.FollowingServers.Contains(server);
        var fittingButton = isTracked ? new ButtonBuilder("Untrack", "anime-selector-untrack") : new ButtonBuilder("Track", "anime-selector-track"); 
        var allEpisodes = new ButtonBuilder("All episodes", "all-episodes"); 
        var componenets = new ComponentBuilder().WithSelectMenu(AnimeSelector(seasonalTitles, server, title.Name)).WithSelectMenu(PageSelector())
            .AddRow(new ActionRowBuilder().WithButton(fittingButton).WithButton(allEpisodes));
        await cmd.RespondAsync(embed: await Screen(cmd.GuildId!.Value, title, isTracked), components: componenets.Build(), ephemeral: true);
    }
    
    async Task<Embed> Screen(ulong serverId, Title title, bool isTracked) {
        var lastEpisode = title.Episodes.OrderBy(e => e.PubDate).Last();
        string? guildName = client.GetGuild(serverId).Name;
        string imgUrl = await GetCoverPic(httpClient, title.Name);
        return new EmbedBuilder {
            ImageUrl = imgUrl,
            Title = title.Name,
            Description = $"{title.LastEpisode} episodes ({title.Episodes.Count} accounted for)\n" +
                          $"Last episode Released at {Timmy.EmbedTime(lastEpisode.PubDate, Timmy.TimeEmbedType.Time)} {Timmy.EmbedTime(lastEpisode.PubDate, Timmy.TimeEmbedType.Remaining)}\n" +
                          $"{(isTracked ? "Is" : "Is not")} tracked on {guildName}",
            Color = new Color(107, 50, 168)
        }.Build();
    }
    
    public static async Task<string> GetCoverPic(HttpClient httpClient, string name) {
        var doc = new HtmlDocument();
        doc.LoadHtml(await (await httpClient.GetAsync("https://subsplease.org/shows/")).Content.ReadAsStringAsync());
        string? href = doc.DocumentNode.SelectSingleNode($"//a[@title='{name}']").GetAttributeValue("href", null);
        doc.LoadHtml(await (await httpClient.GetAsync($"https://subsplease.org{href}")).Content.ReadAsStringAsync());
        return "https://subsplease.org" + doc.DocumentNode.SelectSingleNode("//img").GetAttributeValue("src", null);
    }
    
    SelectMenuBuilder AnimeSelector(IEnumerable<Title> seasonalTitles, ServerData server, string currentlySelecteed) {
        var animeSelector = new SelectMenuBuilder{CustomId = "anime-selector" };
        foreach (var title in seasonalTitles) {
            var similarNameTitle = animeSelector.Options.FirstOrDefault(b => b.Value == title.Name);
            int episodeCount = title.Episodes.Count;
            string isTracked = server.Titles.Contains(title) ? "🟢" : "🔴";
            if (similarNameTitle != null) {
                float similarAnimeEpisodeCount = float.Parse(similarNameTitle.Description.Substring(0, similarNameTitle.Description.Length-9));
                if (similarAnimeEpisodeCount > episodeCount) continue;
                int theIndex = animeSelector.Options.IndexOf(similarNameTitle);
                animeSelector.Options[theIndex] = new SelectMenuOptionBuilder(title.Name, title.Name, episodeCount + " episodes",
                    isDefault: title.Name == currentlySelecteed, emote: new Emoji(isTracked));
            }
            else {
                animeSelector.AddOption(title.Name, title.Name, episodeCount + " episodes", isDefault: title.Name == currentlySelecteed, emote: new Emoji(isTracked));
            }
            if (animeSelector.Options.Count >= 25) break;
        }
        return animeSelector;
    }

    SelectMenuBuilder PageSelector(int currentPage = 1) {
        int seasonalTitles = GetRelevantTitles().Count; 
        int pageCount = (int)MathF.Ceiling(seasonalTitles / 25f);
        var selectMenu = new SelectMenuBuilder{CustomId = "anime-page-selector"};
        for (int i = 1; i <= pageCount; i++) {
            string iString = i.ToString();
            selectMenu.AddOption(new SelectMenuOptionBuilder(iString, iString, isDefault: currentPage == i));
        }
        return selectMenu;
    }
    
    List<Title> GetRelevantTitles(int? page = null) {
        var titles =  ctx.titles.Include(t => t.Episodes).Where(title => title.Episodes.Any()).ToList();
        return titles.Where(t => DateTime.Now - t.Episodes.Last().PubDate < TimeSpan.FromDays(8)).Skip(page != null ? (page.Value - 1) * 25 : 0).ToList();
    }
    async Task HandleAnimeSelectMenu(SocketMessageComponent arg) {
        await arg.DeferAsync();

        // Get page from embed
        var sm = (arg.Message.Components.ElementAt(1).Components.First() as SelectMenuComponent)!;
        int page = int.Parse(sm.Options.First(o => o.IsDefault != null && o.IsDefault.Value).Value);
        
        var seasonalTitles = GetRelevantTitles(page);
        var server = ctx.servers.Where(s => s.Id == arg.GuildId).Include(s => s.Titles).First();
        var title = seasonalTitles.First(t => t.Name == arg.Data.Values.First());
        
        bool isTracked = title.FollowingServers.Contains(server);
        var fittingButton = isTracked ? new ButtonBuilder("Untrack", "anime-selector-untrack") : new ButtonBuilder("Track", "anime-selector-track"); 
        var allEpisodes = new ButtonBuilder("All episodes", "all-episodes"); 
        var componenets = new ComponentBuilder().WithSelectMenu(AnimeSelector(seasonalTitles, server, title.Name)).WithSelectMenu(PageSelector(page))
            .AddRow(new ActionRowBuilder().WithButton(fittingButton).WithButton(allEpisodes));
        var screen = await Screen(arg.GuildId!.Value, title, isTracked);
        await arg.ModifyOriginalResponseAsync(p => {
            p.Embed = screen;
            p.Components = componenets.Build();
        });
    }
    async Task HandlePageSelectMenu(SocketMessageComponent arg) {
        await arg.DeferAsync();
        
        // Get page from request
        int page = int.Parse(arg.Data.Values.First());
        
        var seasonalTitles = GetRelevantTitles(page);
        var server = ctx.servers.Where(s => s.Id == arg.GuildId).Include(s => s.Titles).First();
        var title = seasonalTitles.First();        
        
        bool isTracked = title.FollowingServers.Contains(server);
        var fittingButton = isTracked ? new ButtonBuilder("Untrack", "anime-selector-untrack") : new ButtonBuilder("Track", "anime-selector-track"); 
        var allEpisodes = new ButtonBuilder("All episodes", "all-episodes"); 
        var componenets = new ComponentBuilder().WithSelectMenu(AnimeSelector(seasonalTitles, server, title.Name)).WithSelectMenu(PageSelector(page))
            .AddRow(new ActionRowBuilder().WithButton(fittingButton).WithButton(allEpisodes));
        var screen = await Screen(arg.GuildId!.Value, title, isTracked);
        await arg.ModifyOriginalResponseAsync(p => {
            p.Embed = screen;
            p.Components = componenets.Build();
        });
    }
    
    async Task ShowAllEpisodes(SocketMessageComponent arg) {
        string name = arg.Message.Embeds.First().Title;
        var title = ctx.titles.Include(t => t.Episodes).First(t => t.Name == name);
        
        var embed = new EmbedBuilder {
            Title = $"all of the episodes for \"{title.Name}\":",
            Color = new Color(69, 136, 230),
            ThumbnailUrl = await GetCoverPic(httpClient, title.Name),
        };
        StringBuilder sb = new();
        foreach (var episode in title.Episodes.TakeLast(12)) {
            sb.AppendLine($"[__**Episode {episode.Number}**__]({"https://nyaasi.final-spark.eu" + episode.Link[15..]})");
        }
        embed.AddField("**Download torrent:**", sb.ToString());
        embed.WithFooter(DateTime.Now.ToString("ddd, dd MMM yyy HH:mm:ss"));
        await arg.RespondAsync(embed: embed.Build());
    }
    async Task GetMissingEpisodes(string titleName) {
        var doc = new HtmlDocument();
        var dbTitle = ctx.titles.Include(title => title.Episodes).First(title => title.Name == titleName);
        string fullUrl = "https://subsplease.org/shows/" + new Regex("[^a-zA-Z0-9]+").Replace(titleName, "-");
        doc.LoadHtml(await (await httpClient.GetAsync(fullUrl)).Content.ReadAsStringAsync());
        string? sid = doc.DocumentNode.SelectSingleNode("//table[@id='show-release-table']").GetAttributeValue("sid", null);
        var stuff = JsonConvert.DeserializeObject<Episodes>(await (await httpClient.GetAsync($"https://subsplease.org/api/?f=show&tz=Europe/Moscow&sid={sid}")).Content.ReadAsStringAsync())!;
        try {
            foreach (var episode in stuff.episodes.Values.Where(ep => float.TryParse(ep.episode, out _)).TakeLast(26)) {
                string? dlLink = episode.downloads.Last().torrent;
                float ep = float.Parse(new Regex(@"v\d+").Replace(episode.episode, ""));
                if (dbTitle.Episodes.Exists(e => Math.Abs(e.Number - ep) < 0.1f)) continue;
                dbTitle.Episodes.Add(
                    new Episode {
                        Link = dlLink,
                        Group = "SubsPlease",
                        Number = ep,
                        PubDate = DateTime.Parse(episode.release_date, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal),
                    });
            }
            await ctx.SaveChangesAsync();
        }
        catch (Exception e) {
            Console.WriteLine(e);
            throw;
        }
    }
}