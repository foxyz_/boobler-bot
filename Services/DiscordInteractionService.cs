﻿using BooblerBot.Services.InteractionHandlers;
using Discord;
using Discord.WebSocket;

namespace BooblerBot.Services; 

public class DiscordInteractionService {
    readonly DiscordSocketClient client;
    readonly IServiceScopeFactory scopeFactory;

    public DiscordInteractionService(DiscordSocketClient client, IServiceScopeFactory scopeFactory) {
        this.client = client;
        this.scopeFactory = scopeFactory;
        client.GuildAvailable += InitializeCommands;
        
        client.SlashCommandExecuted += SlashCommandExecuted;
        client.ButtonExecuted += ButtonExecuted;
        client.SelectMenuExecuted += SelectMenuExecuted;
    }

    static async Task InitializeCommands(SocketGuild guild) {
        await guild.BulkOverwriteApplicationCommandAsync(new ApplicationCommandProperties[] {
            new SlashCommandBuilder().WithName("week").WithDescription("Shows weekly anime episode schedule").Build(),
            new SlashCommandBuilder().WithName("sd").WithDescription("Stable diffusion related commands")
                .AddOption(new SlashCommandOptionBuilder().WithName("model").WithDescription("Select currently used model").WithType(ApplicationCommandOptionType.SubCommand))
                .AddOption(new SlashCommandOptionBuilder().WithName("generate").WithDescription("Generate picture with the currently chosen preset").WithType(ApplicationCommandOptionType.SubCommandGroup)
                    .AddOption(new SlashCommandOptionBuilder().WithName("from-text").WithDescription("Genereate an image from just the text input").WithType(ApplicationCommandOptionType.SubCommand)
                        .AddOption("prompt", ApplicationCommandOptionType.String, "The prompt for the picture")
                        .AddOption("negative-prompt", ApplicationCommandOptionType.String, "What the ai should avoid")
                        .AddOption("cfg-scale", ApplicationCommandOptionType.Number, "How much the AI should follow the prompt")
                        .AddOption("steps", ApplicationCommandOptionType.Integer, "Amount of the iterations for the picture")
                        .AddOption("seed", ApplicationCommandOptionType.Integer, "Seed for the picture used in generation")
                        .AddOption("width", ApplicationCommandOptionType.Integer, "width for the generated picture")
                        .AddOption("height", ApplicationCommandOptionType.Integer, "height for the generated picture"))
                    .AddOption(new SlashCommandOptionBuilder().WithName("from-image").WithDescription("Generate an image from image and a text input").WithType(ApplicationCommandOptionType.SubCommand)
                        .AddOption("image-url", ApplicationCommandOptionType.String, "The image used as a base for generation", true)
                        .AddOption("denoising-strength", ApplicationCommandOptionType.Number, "How strong is the base image")
                        .AddOption("prompt", ApplicationCommandOptionType.String, "The prompt for the picture")
                        .AddOption("negative-prompt", ApplicationCommandOptionType.String, "What the ai should avoid")
                        .AddOption("cfg-scale", ApplicationCommandOptionType.Number, "How much the AI should follow the prompt")
                        .AddOption("steps", ApplicationCommandOptionType.Integer, "Amount of the iterations for the picture")
                        .AddOption("seed", ApplicationCommandOptionType.Integer, "Seed for the picture used in generation"))
                ).AddOption(new SlashCommandOptionBuilder().WithName("update-preset").WithDescription("Update currrently used preset").WithType(ApplicationCommandOptionType.SubCommand)
                    .AddOption("prompt", ApplicationCommandOptionType.String, "The prompt for the picture")
                    .AddOption("image-url", ApplicationCommandOptionType.String, "The image used as a base for generation")
                    .AddOption("denoising-strength", ApplicationCommandOptionType.Number, "How strong is the base image")
                    .AddOption("negative-prompt", ApplicationCommandOptionType.String, "What the ai should avoid")
                    .AddOption("cfg-scale", ApplicationCommandOptionType.Number, "How much the AI should follow the prompt")
                    .AddOption("steps", ApplicationCommandOptionType.Integer, "Amount of the iterations for the picture")
                    .AddOption("seed", ApplicationCommandOptionType.String, "Seed for the picture used in generation")
                ).AddOption(new SlashCommandOptionBuilder().WithName("default-preset").WithDescription("Use the default preset").WithType(ApplicationCommandOptionType.SubCommand)
                ).AddOption(new SlashCommandOptionBuilder().WithName("show-preset").WithDescription("Show the currently used preset").WithType(ApplicationCommandOptionType.SubCommand)
                    .AddOption("ephemeral", ApplicationCommandOptionType.Boolean, "Will the others see your preset")
                ).Build(),
            new SlashCommandBuilder().WithName("set-announcement").WithDescription("Set this channel as announcement channel.").Build(),
            new SlashCommandBuilder().WithName("set-vk").WithDescription("Set this channel as vk channel.").Build(),
            new SlashCommandBuilder().WithName("profile").WithDescription("Show profile")
                .AddOption("user", ApplicationCommandOptionType.User, "Which user", false).Build(),
            new SlashCommandBuilder().WithName("leaderboard").WithDescription("Show leaderboard").Build(),
            new SlashCommandBuilder().WithName("shop").WithDescription("Show shop UI").Build(),
            new SlashCommandBuilder().WithName("gel").WithDescription("Booba").Build(),
            new SlashCommandBuilder().WithName("weeb").WithDescription("Weeb related commands")
                // .AddOption(new SlashCommandOptionBuilder().WithName("1").WithDescription("Test1").WithType(ApplicationCommandOptionType.SubCommand))
                // .AddOption(new SlashCommandOptionBuilder().WithName("2").WithDescription("Test2").WithType(ApplicationCommandOptionType.SubCommand))
                .AddOption(new SlashCommandOptionBuilder().WithName("menu").WithDescription("Show menu with all current Seasonal Anime").WithType(ApplicationCommandOptionType.SubCommand)).Build(),
            new SlashCommandBuilder().WithName("check").WithDescription("Checker stuff")
                .AddOption(new SlashCommandOptionBuilder().WithName("append").WithDescription("Add a title name to check for").WithType(ApplicationCommandOptionType.SubCommand)
                    .AddOption("title", ApplicationCommandOptionType.String, "Title name", true))
                .AddOption(new SlashCommandOptionBuilder().WithName("remove").WithDescription("Remove entries from the check").WithType(ApplicationCommandOptionType.SubCommand)
                    .AddOption("title", ApplicationCommandOptionType.String, "Title name", true))
                    .AddOption(new SlashCommandOptionBuilder().WithName("view").WithDescription("View the check entries").WithType(ApplicationCommandOptionType.SubCommand))
                .AddOption(new SlashCommandOptionBuilder().WithName("custom").WithDescription("add or remove custom entries in /week").WithType(ApplicationCommandOptionType.SubCommandGroup)
                    .AddOption(new SlashCommandOptionBuilder().WithName("add").WithDescription("add a custom entry").WithType(ApplicationCommandOptionType.SubCommand)
                        .AddOption("title-name", ApplicationCommandOptionType.String, "Title name", isRequired:true)
                        .AddOption("pub-date", ApplicationCommandOptionType.String, "Last episode Pubdate (2022-08-04 18:48)", isRequired:true))
                    .AddOption(new SlashCommandOptionBuilder().WithName("remove").WithDescription("remove a custom entry").WithType(ApplicationCommandOptionType.SubCommand)
                        .AddOption("title-name", ApplicationCommandOptionType.String, "Title name", isRequired:true))
                ).Build()
        });
    }

    Task SlashCommandExecuted(SocketSlashCommand arg) {
        Task t = Task.Factory.StartNew(async () => {
            await using var scope = scopeFactory.CreateAsyncScope();
            var cmdService = scope.ServiceProvider.GetService<InteractionHandler>();
            await cmdService.HandleSlashCommand(arg);
        });
        return Task.CompletedTask;
    }
    Task ButtonExecuted(SocketMessageComponent arg) {
        Task t = Task.Factory.StartNew(async () => {
            await using var scope = scopeFactory.CreateAsyncScope();
            var cmdService = scope.ServiceProvider.GetService<InteractionHandler>();
            await cmdService.HandleButton(arg);
        });
        return Task.CompletedTask;
    }
    Task SelectMenuExecuted(SocketMessageComponent arg) {
        Task t = Task.Factory.StartNew(async () => {
            await using var scope = scopeFactory.CreateAsyncScope();
            var cmdService = scope.ServiceProvider.GetService<InteractionHandler>();
            await cmdService.HandleSelectMenu(arg);
        });
        return Task.CompletedTask;
    }
}