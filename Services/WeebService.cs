﻿using System.Globalization;
using System.Text.RegularExpressions;
using System.Timers;
using System.Xml;
using System.Xml.Serialization;
using BooblerBot.Context;
using BooblerBot.Models.API_Models;
using BooblerBot.Services.InteractionHandlers;
using Discord;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using Timer = System.Timers.Timer;

namespace BooblerBot.Services;


public interface IRssPooler {
    public Task<List<Episode>> GetEpisodes();
}

public class SubsPleasePooler : IRssPooler {
    static readonly List<string> ReportedStuff = new();
    public Task<List<Episode>> GetEpisodes() {
        var serializer = new XmlSerializer(typeof(Rss));
        var doc = new XmlDocument();
        try { 
            doc.Load("https://subsplease.org/rss/?t&r=1080");
        }
        catch (Exception e) {
            //throw Timmy.ConsoleThrower($"There were several difficulties doing stuff '{e.Message}'");
            throw new Exception(e.Message);
        }
        string xml = doc.InnerXml;
        using var reader = new StringReader(xml);
        var items = ((Rss) serializer.Deserialize(reader)!).Channel.Item;
        //var items = test.Channel.Item.Where(x => BotFiles.LoadSettings().PassList.Any(y => x.Title.Contains(y, Lc))).ToList();
        var groupRegex = new Regex(@"\[(\w*)\] ");
        var episodes = new List<Episode>();
        foreach (var item in items) {
            var groupMatch = groupRegex.Match(item.Title);
            if (groupMatch.Success) {
                switch (groupMatch.Groups[1].Value) {
                    case "SubsPlease" or "EraiRaws":
                        var subsPleaseTitleRegex = new Regex(@"\[\w*\]\s(.*?)\s-\s(\d+(?:\.\d+)?)");
                        var titleMatch = subsPleaseTitleRegex.Match(item.Title);
                        if (!titleMatch.Success || !titleMatch.Groups[1].Success || !titleMatch.Groups[2].Success) {
                            if (ReportedStuff.Contains(item.Title)) continue;
                            Console.WriteLine($"Something has escaped a match from '{item.Title}'"); // TODO handle batch releases and OVAs
                            ReportedStuff.Add(item.Title);
                            continue;
                        }
                        episodes.Add(new Episode {
                            Title = new Title(titleMatch.Groups[1].Value),
                            Number = float.Parse(titleMatch.Groups[2].Value),
                            Group = groupMatch.Groups[1].Value,
                            PubDate = DateTime.Parse(item.PubDate, CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal),
                            Guid = item.Guid.Text,
                            Link = item.Link
                        });
                        break;
                    default: Console.WriteLine($"Unhandled group '{groupMatch.Groups[1].Value}' in {item.Title}"); break;
                }
            }
            else {
                Console.WriteLine($"Error parsing '{item.Title}' group");
            }
        }
        return Task.FromResult(episodes);
    }
}
public class WeebService {
    readonly IServiceScopeFactory scopeFactory;
    readonly HttpClient http;
    readonly IRssPooler pooler;
    readonly DiscordSocketClient client;
    readonly DiscordInteractionService discordInteractionService;
    readonly Timer checkTimer = new();
    
    public Dictionary<ulong, SocketTextChannel> AnnouncementChannels = null!;

    public WeebService(IRssPooler pooler, DiscordSocketClient client, DiscordInteractionService discordInteractionService, IServiceScopeFactory scopeFactory, HttpClient http) {
        this.pooler = pooler;
        this.client = client;
        this.discordInteractionService = discordInteractionService;
        this.scopeFactory = scopeFactory;
        this.http = http;
        _ = SyncWithDb();
    }

    async Task SyncWithDb() {
        Console.WriteLine("Weeb service: Connecting to Discord...");
        if (client.ConnectionState != ConnectionState.Connected) {
            checkTimer.Stop();
            var signal = new SemaphoreSlim(0, 1);
            client.Ready += Handler;
            await signal.WaitAsync();
            
            Task Handler() {
                checkTimer.Start();
                client.Ready -= Handler;
                signal.Release();
                return Task.CompletedTask;
            }
        }
        Console.WriteLine("Weeb service: Connected to Discord");
        
        Console.WriteLine("Weeb service: Loading DB data...");
        AnnouncementChannels = new Dictionary<ulong, SocketTextChannel>();
        
        using var scope = scopeFactory.CreateScope();
        var ctx = scope.ServiceProvider.GetRequiredService<MyDbContext>();
        
        foreach (var server in client.Guilds) {
            var dbEntry = ctx.servers.FirstOrDefault(data => data.Id == server.Id); 
            if (dbEntry == null) {
                ctx.servers.Add(new ServerData(server.Id)); // TODO Make ensure created method
            }
            else {
                if (dbEntry.AnnouncementChannelId != null) {
                    var channel = (SocketTextChannel?) server.Channels.FirstOrDefault(channel => channel.Id == dbEntry.AnnouncementChannelId);
                    if (channel == null) {
                        Console.WriteLine("Weeb service: Announcement channel not found!");
                        continue;
                    }
                    AnnouncementChannels.Add(server.Id, channel);
                }
            }
        }
        await ctx.SaveChangesAsync();        
        Console.WriteLine("Weeb service: Start");
        
        checkTimer.Interval = TimeSpan.FromMinutes(5).TotalMilliseconds;
        checkTimer.AutoReset = true;
        checkTimer.Start();
        checkTimer.Elapsed += CheckForEpisodes;
        CheckForEpisodes();
    }

    async void CheckForEpisodes(object? source = null, ElapsedEventArgs? eventArgs = null) {
        if (client.ConnectionState != ConnectionState.Connected) {
            checkTimer.Stop();
            var signal = new SemaphoreSlim(0, 1);
            client.Ready += Handler;
            await signal.WaitAsync();
            
            Task Handler() {
                checkTimer.Start();
                client.Ready -= Handler;
                signal.Release();
                return Task.CompletedTask;
            }
        }
        using var scope = scopeFactory.CreateScope();
        var db = scope.ServiceProvider.GetRequiredService<MyDbContext>();
        
        var rssEpisodes = await pooler.GetEpisodes();
        var dbTitles = db.titles
            .Include(dbTitle => dbTitle.Episodes.OrderByDescending(e => e.Number).Take(3));

        foreach (var episode in rssEpisodes) { 
            try {
                var dbTitle = await dbTitles.FirstOrDefaultAsync(title => title.Name == episode.Title.Name);
                if (dbTitle == null) {
                    var newDbTitle = new Title(episode.Title.Name);
                    await db.titles.AddAsync(newDbTitle);
                    dbTitle = newDbTitle;
                }

                var lastDbEpisode = dbTitle.Episodes.MaxBy(e => e.Number);
                if (lastDbEpisode != null && episode.Number <= lastDbEpisode.Number) continue;
                episode.Title = dbTitle;
                dbTitle.Episodes.Add(episode);

                bool isFirstEpisode = dbTitle.Episodes.Count == 1 && episode.Number <= 1;
                var posting = isFirstEpisode ? AnnounceFirstEpisode(db, episode) : AnnounceNewEpisode(db, episode);
                await posting;
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
        await db.SaveChangesAsync();
    }

    async Task AnnounceFirstEpisode(MyDbContext context, Episode episode) { // Episode has dbTitle withitn the context
        Console.WriteLine($"Came out first episode of {episode.Title.Name}");
        
        foreach (var announcementChannel in AnnouncementChannels.Values) {
            var dbServer = context.servers.Include(s => s.Titles).First(server => server.Id == announcementChannel.Guild.Id);
            string? guildName = announcementChannel.Guild.Name;
            bool isTracked = dbServer.Titles.Any(title => title.Name == episode.Title.Name);
            
            var embed = new EmbedBuilder {
                Title = $"Came out the first episode of \"{episode.Title.Name}\"",
                Color = new Color(69, 136, 230),
                Description = $"{(isTracked ? "🟢 Is" : "🔴 Is not yet")} tracked on {guildName}",
                ThumbnailUrl = await InteractionHandler.GetCoverPic(http, episode.Title.Name)
            };
            
            var fittingButton = isTracked ? new ButtonBuilder("Untrack", "first-episode-untrack") : new ButtonBuilder("Track", "first-episode-track");

            embed.AddField("**Torrent**", $"[__**Download!**__]({"https://nyaasi.final-spark.eu" + episode.Link[15..]})");
            embed.WithFooter(episode.PubDate.ToString("ddd, dd MMM yyy HH:mm:ss"));
            var components = new ComponentBuilder().WithButton(fittingButton).Build();
            await announcementChannel.SendMessageAsync(embed: embed.Build(), components: components);
        }
    }
    
    async Task AnnounceNewEpisode(MyDbContext context, Episode episode) { // Episode has dbTitle withitn the context
        Console.WriteLine($"Came out episode {episode.Number} of {episode.Title.Name}");
        foreach (var announcementChannel in AnnouncementChannels.Values) {
            var dbServer = context.servers.Include(s => s.Titles).First(server => server.Id == announcementChannel.Guild.Id);
            if (!dbServer.Titles.Contains(episode.Title)) return;
            var embed = new EmbedBuilder {
                Title = $"Came out episode {episode.Number} of \"{episode.Title.Name}\"",
                Color = new Color(69, 136, 230),
                ThumbnailUrl = await InteractionHandler.GetCoverPic(http, episode.Title.Name),
            };
            embed.AddField("**Torrent**", $"[__**Download!**__]({"https://nyaasi.final-spark.eu" + episode.Link[15..]})");
            embed.WithFooter(episode.PubDate.ToString("ddd, dd MMM yyy HH:mm:ss"));
            await announcementChannel.SendMessageAsync(embed: embed.Build());
        }
    }

}