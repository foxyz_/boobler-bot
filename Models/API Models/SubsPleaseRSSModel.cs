﻿#nullable disable

using System.Xml.Serialization;
using Newtonsoft.Json;

namespace BooblerBot.Models.API_Models
{ 
    // using System.Xml.Serialization;
    // XmlSerializer serializer = new XmlSerializer(typeof(Rss));
    // using (StringReader reader = new StringReader(xml))
    // {
    //    var test = (Rss)serializer.Deserialize(reader);
    // }

    
    // JSON STUFF
    

    public class Episodes
    {
        [JsonProperty("episode")]
        public Dictionary<string, ApiEpisode> episodes { get; set; }
    }
    public class Download
    {
        public string res { get; set; }
        public string torrent { get; set; }
        public string magnet { get; set; }
        public string xdcc { get; set; }
    }
    public class ApiEpisode
    {
        public string time { get; set; }
        public string release_date { get; set; }
        public string show { get; set; }
        public string episode { get; set; }
        public List<Download> downloads { get; set; }
    }
    
    // XML STUFF
    [XmlRoot(ElementName="link")]
    public class Link { 

        [XmlAttribute(AttributeName="href")] 
        public string Href { get; set; } 

        [XmlAttribute(AttributeName="rel")] 
        public string Rel { get; set; } 

        [XmlAttribute(AttributeName="type")] 
        public string Type { get; set; } 
    }

    [XmlRoot(ElementName="guid")]
    public class Guid { 

        [XmlAttribute(AttributeName="isPermaLink")] 
        public bool IsPermaLink { get; set; } 

        [XmlText] 
        public string Text { get; set; } 
    }

    [XmlRoot(ElementName="item")]
    public class Item { 

        [XmlElement(ElementName="title")] 
        public string Title { get; set; } 

        [XmlElement(ElementName="link")] 
        public string Link { get; set; } 

        [XmlElement(ElementName="guid")] 
        public Guid Guid { get; set; } 

        [XmlElement(ElementName="pubDate")] 
        public string PubDate { get; set; } 

        [XmlElement(ElementName="category")] 
        public string Category { get; set; } 

        [XmlElement(ElementName="size")] 
        public string Size { get; set; } 
    }

    [XmlRoot(ElementName="channel")]
    public class Channel { 

        [XmlElement(ElementName="title")] 
        public string Title { get; set; } 

        [XmlElement(ElementName="description")] 
        public string Description { get; set; } 

        [XmlElement(ElementName="link")] 
        public List<string> Link { get; set; } 

        [XmlElement(ElementName="item")] 
        public List<Item> Item { get; set; } 
    }

    [XmlRoot(ElementName="rss")]
    public class Rss { 

        [XmlElement(ElementName="channel")] 
        public Channel Channel { get; set; } 

        [XmlAttribute(AttributeName="version")] 
        public double Version { get; set; } 

        [XmlAttribute(AttributeName="atom")] 
        public string Atom { get; set; } 

        [XmlAttribute(AttributeName="subsplease")] 
        public string Subsplease { get; set; } 

        [XmlText] 
        public string Text { get; set; } 
    }
}