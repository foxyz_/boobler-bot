﻿using Discord;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;

namespace BooblerBot; 

public static class Files {
    public class Config {
        const string ConfigFilePath = "cfg.json";
        public bool IsDevelopment { get; private set; } = false;
        public string MainlineToken { get; private set; } = "";
        public string DevToken { get; private set; } = "";
        public bool UsePostgres { get; } = false;
        public string PosgresConnectionString { get; private set; } = "";
        public string SqLiteConnectionString { get; private set; } = "";

        public static Config Load() {
            Config config;
            if (!File.Exists(ConfigFilePath)) config = CreateConfigFile();
            else {
                try { 
                    config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(ConfigFilePath))!;
                }
                catch (Exception e) {
                    Console.WriteLine($"{e.Message}\n\n{e}");
                    throw;
                }
            }
            return config;
        }
        public void Save() { 
            File.WriteAllText(ConfigFilePath, JsonConvert.SerializeObject(this));
        }

        static Config CreateConfigFile() {
            var config = new Config();
            
            config.SetIsDevelopment();
            config.SetAppropriateToken();
            config.SetAppropriateConnectionString();
            
            var sw = File.CreateText(ConfigFilePath);
            sw.Write(JsonConvert.SerializeObject(config));
            sw.Flush();
            sw.Close();

            return config;

        }
        void SetIsDevelopment() {
            while (true) {
                Console.WriteLine("Start in development mode or mainline?\n d = Development mode\n m = Mainline mode");
                string? result = Console.ReadLine();
                if (string.IsNullOrEmpty(result)) continue;
                result = result.ToLower();
                if (result == "d") {
                    IsDevelopment = true;
                    break;
                }
                if (result != "m") continue;
                IsDevelopment = false;
                break;
            }
        }            
        public void SetAppropriateToken() {
            while (true) {
                Console.Write($"Input a valid {(IsDevelopment ? "dev" : "mainline")} discord bot token: ");
                string? result = Console.ReadLine();
                if (string.IsNullOrEmpty(result)) continue;
                try { 
                    TokenUtils.ValidateToken(TokenType.Bot, result);
                } catch (Exception e) {
                    Console.WriteLine(e.Message);
                    continue;
                }
                if (IsDevelopment) {
                    DevToken = result;
                } else {
                    MainlineToken = result;
                }
                break;
            }
        }
        void SetAppropriateConnectionString() {
            while (true) {
                Console.Write($"Input a valid {(UsePostgres ? "Postgres" : "SqLite")} db connection string: ");
                string? result = Console.ReadLine();
                if (string.IsNullOrEmpty(result)) continue;
                if (UsePostgres) {
                    throw new NotImplementedException("You forgot to code postgres check goober");
                    PosgresConnectionString = result;
                } else {
                    try {
                        new SqliteConnectionStringBuilder(result);
                    }
                    catch (Exception e) {
                        Console.WriteLine(e.Message);
                        continue;
                    }
                    SqLiteConnectionString = result;
                }
                break;
            }
        }
        [JsonConstructor]
        Config(bool isDevelopment, string mainlineToken, string devToken, bool usePostgres, string posgresConnectionString, string sqLiteConnectionString) {
            IsDevelopment = isDevelopment;
            MainlineToken = mainlineToken;
            DevToken = devToken;
            UsePostgres = usePostgres;
            PosgresConnectionString = posgresConnectionString;
            SqLiteConnectionString = sqLiteConnectionString;
        }
        Config() { }
    }
}