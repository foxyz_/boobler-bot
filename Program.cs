using System.Net;
using BooblerBot;
using BooblerBot.Context;
using BooblerBot.Services;
using BooblerBot.Services.InteractionHandlers;
using Discord;
using Discord.Net;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;


DiscordSocketClient client = new();
HttpClient requestClient = new();

var config = Files.Config.Load();

var signal = new SemaphoreSlim(0,1);

client.Log += Log;
client.Ready += Ready;

await client.LoginAsync(TokenType.Bot, config.IsDevelopment ? config.DevToken : config.MainlineToken);
await client.StartAsync();

Task Ready() {
    signal.Release();
    client.Ready -= Ready;
    return Task.CompletedTask;
}

async Task Log(LogMessage log) {
    switch (log.Exception) {
        case HttpException {HttpCode: HttpStatusCode.Unauthorized} or ArgumentException:
            Console.WriteLine($"Discord client: Invalid token Message({log.Message}) Exception({log.Exception.Message})");
            config.SetAppropriateToken();
            config.Save();
            client.Log -= Log;
            client.Ready -= Ready;
            client = new DiscordSocketClient();
            client.Log += Log;
            client.Ready += Ready;
            await client.LoginAsync(TokenType.Bot, config.IsDevelopment ? config.DevToken : config.MainlineToken);
            await client.StartAsync();
            break;
        default: {
            if (log.Exception != null) {
                Console.WriteLine($"Discord client: Exception({log.Exception.Message} || {log.Exception}) {log.Message}");
            } else if ((int)log.Severity < 3) {
                Console.WriteLine($"Discord client: ({log.Severity}) {log.Message}");
            }
            break;
        }
    }
}

await signal.WaitAsync();
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddSingleton(client);
builder.Services.AddSingleton(requestClient);

if (config.UsePostgres) {
    throw new NotImplementedException("Goober, you forgot to code that in.");
}
else {
    builder.Services.AddDbContext<MyDbContext>(options => options.UseSqlite(config.SqLiteConnectionString));
}

builder.Services.AddSingleton<IRssPooler, SubsPleasePooler>();
builder.Services.AddSingleton<WeebService>();
builder.Services.AddSingleton<DiscordInteractionService>();
builder.Services.AddScoped<InteractionHandler>();
builder.Services.AddSignalR();

var app = builder.Build();

app.MapGet("/", () => "Hello World!");
app.MapHub<ChatHub>("/chatHub");
app.UseSwaggerUI();

var scope = app.Services.GetRequiredService<IServiceScopeFactory>().CreateScope();
var ctx = scope.ServiceProvider.GetService<MyDbContext>();
ctx!.Database.EnsureCreated();
scope.Dispose();

app.Services.GetRequiredService<WeebService>();

app.Run();

